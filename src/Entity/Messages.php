<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessagesRepository")
 */
class Messages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="messages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Conversations", inversedBy="conv")
     * @ORM\JoinColumn(nullable=true)
     */
    private $conversation;

    /**
     * Messages constructor.
     * @param $content, $conversation, $user_id
     */
    public function __construct($content, $conversation, $user)
    {

         $this->content = $content;
         $this->user = $user;
         $this->date = new \DateTime("now");
        if ($conversation !== NULL){
            $this->conversation = $conversation;
    }

      /*  $now = \DateTime::createFromFormat( 'yy-mm-dd hh:mm:ss', time());
        $now->setTimezone(new \DateTimeZone('Europe/Paris'));
        $this->setDate($now);*/

        $this->setUser($this->user);
        $this->setContent($this->content);
        $this->setDate($this->date);
    }


    public function getId()
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?Users
    {
        return $this->user;
    }

    public function setUser(?Users $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getConversation(): ?Conversations
    {
        return $this->conversation;
    }

    public function setConversation(?Conversations $conversation): self
    {
        $this->conversation = $conversation;

        return $this;
    }
}
