<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ConversationsRepository")
 */
class Conversations
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Messages", mappedBy="conversation")
     */
    private $conv;

    public function __construct()
    {
        $this->conv = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Messages[]
     */
    public function getConv(): Collection
    {
        return $this->conv;
    }

    public function addConv(Messages $conv): self
    {
        if (!$this->conv->contains($conv)) {
            $this->conv[] = $conv;
            $conv->setConversation($this);
        }

        return $this;
    }

    public function removeConv(Messages $conv): self
    {
        if ($this->conv->contains($conv)) {
            $this->conv->removeElement($conv);
            // set the owning side to null (unless already changed)
            if ($conv->getConversation() === $this) {
                $conv->setConversation(null);
            }
        }

        return $this;
    }
}
