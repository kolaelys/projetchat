<?php

namespace App\Controller;

use App\Entity\Conversations;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ConversationController extends Controller
{

    public $serializer = null;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/conversations", name="conversations", methods="GET")
     */
    public function index()
    {
        $conversations = $this->getDoctrine()->getRepository(Conversations::class)
            ->findAll();

        $data = $this->serializer->serialize($conversations, 'json');

        return new Response($data);
    }

    /**
     * @Route("/conversation/add", name="add_conversation", methods="POST")
     */
    public function add(){
        $conversation= new Conversations();
        $request = Request::createFromGlobals();
        $title= $request->request->get('title');
        $conversation->setTitle($title);

        $em = $this->getDoctrine()->getManager();
        $em->persist($conversation);
        $em->flush();
        $data= ['success'=> 'id'.$conversation->getId(), 'title'=> 'title'.$title.'created'];
        return new JsonResponse($data);
    }

    /**
     * @Route("/conversation/show/{id}/messages", name="showMessageConv", methods="GET")
     */
    public function showMessage(Conversations $conversations){
        $messages= $conversations->getConv();
        $data= $this->serializer->serialize($messages, 'json');
        return new Response($data);
    }


    /**
     * @Route("/conversation/show/{id}", name="show_conversation", methods="GET")
     */
    public function show(Conversations $conversations){
        $messages= $conversations->getConv();
        $data= ['success'=> 'conversation '.$conversations->getTitle()];
        return new JsonResponse($data);
    }

    /**
     * @Route("/conversation/update/{id}", name="updateConversation",methods="PUT")
     */
    public function update(Conversations $conversation):Response{

        $em = $this->getDoctrine()->getManager();

        if (!$conversation) {
            throw $this->createNotFoundException(
                'No product found for id '.$conversation->getId()
            );
        }
        $request = Request::createFromGlobals();
        $title = $request->request->get('title');
        $conversation->setTitle($title);
        $data=['success'=>'conversation updated', 'new title'=>$conversation->getTitle()];
        $em->persist($conversation);
        $em->flush();

        return new JsonResponse($data);
    }


    /**
     * @Route("/conversation/delete/{id}", name="deleteConversation",  methods="DELETE")
     */
    public function delete(Conversations $conversation){
        $em= $this->getDoctrine()->getManager();
        $data=['success'=>'conversation deleted: '.$conversation->getTitle()];
        $em->remove($conversation);
        $em->flush();
        return new JsonResponse($data);
    }



}
