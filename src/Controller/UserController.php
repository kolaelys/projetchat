<?php

namespace App\Controller;

use App\Entity\Users;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class UserController extends Controller
{
    public $serializer = null;

    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/users", name="users")
     */
    public function index()
    {
        $users = $this->getDoctrine()->getRepository(Users::class)
            ->findAll();
        $data = $this->serializer->serialize($users, 'json');

        return new Response($data);
    }

    /**
     * @Route("/user/add", name="addUser", methods="POST")
     */
    public function add(){
        $user= new Users();
        $request = Request::createFromGlobals();
        $pseudo= $request->request->get('pseudo');
        $user->setPseudo($pseudo);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        $data= ['success'=> 'User added: ','id'=>$user->getId(),'pseudo'=> $pseudo];

        return new JsonResponse($data);

    }


    /**
     * @Route("/user/{id}", name="showUser", methods="GET")
     */


    public function show(Users $user)
    {
        $data= ['success'=> 'users '. 'list', 'id'=>$user->getId(), 'pseudo'=>$user->getPseudo()];
        return new JsonResponse($data);
    }




    /**
     * @Route("/user/update/{id}", name="updateUser",methods="PUT")
     */
    public function update(Users $user):Response{

        $em = $this->getDoctrine()->getManager();

        if (!$user) {
            throw $this->createNotFoundException(
                'No product found for id '.$user->getId()
            );
        }
        $request = Request::createFromGlobals();
        $pseudo= $request->request->get('pseudo');
        $user->setPseudo($pseudo);
        $data=['success'=>'user updated', 'new pseudo'=>$user->getPseudo()];
        $em->persist($user);
        $em->flush();

        return new JsonResponse($data);
    }



    /**
     * @Route("/user/delete/{id}", name="deleteUser",  methods="DELETE")
     */

    public function delete(Users $user){
        $em= $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        $data=['success'=>'user deleted', 'user'=>$user->getPseudo()];
        return new JsonResponse($data);

    }

}

