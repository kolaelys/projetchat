<?php

namespace App\Controller;

use App\Entity\Conversations;
use App\Entity\Messages;
use App\Entity\Users;
use App\Repository\MessagesRepository;
use Doctrine\DBAL\Types\DateType;
use phpDocumentor\Reflection\Types\Null_;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @property Serializer serializer
 */
class MessagesController extends Controller
{
    public function __construct()
    {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }


    /**
     * @Route("/messages", name="messages")
     */
    public function index()
    {
        $messages = $this->getDoctrine()->getRepository(Messages::class)
            ->findAll();

        $data = $this->serializer->serialize($messages, 'json');
        return new Response($data);
    }

    /**
     * @Route("/message/add", name="addMessage", methods="POST")
     */
    public function add ():Response{
        $id= null;
        $request = Request::createFromGlobals();
        $data = $request->getContent();
        $data= json_decode($data,true);
        $user_id= $data['user'];
        $content= $data['content'];
        // $content= $request->request->get('content');
        // $user_id= $request->request->get('user');

        if($id !== NULL) {
            $conversation = $this->getDoctrine()->getRepository(Conversations::class)->find($id);
        }else {
            $conversation = $id;
        }

        $user= $this->getDoctrine()->getRepository(Users::class)->find($user_id);

        $message= new Messages($content,$conversation, $user);

        $em = $this->getDoctrine()->getManager();
        $em->persist($message);
        $em->flush();

        $json = $this->serializer->serialize($message, 'json');
        return new Response($json);
    }

    /**
     * @Route("/message/delete/{id}", name="deleteMessage",  methods="DELETE")
     * @param Messages $messages
     * @return JsonResponse
     */

    public function delete(Messages $messages){
        $em= $this->getDoctrine()->getManager();
        $em->remove($messages);
        $em->flush();
        $data=['success'=>'deleted'];
        return new JsonResponse($data);

    }


    /**
     * @Route("/message/update", name="updateMessage",methods="PATCH")
     */
    public function update(Request $request){

        $message = $request->getContent();
        $message= json_decode($message,true);

        $id= $message['id'];
        $content= $message['content'];

       $this->getDoctrine()->getRepository(Messages::class)->updateMsg($id, $content);

        $json = $this->serializer->serialize('done', 'json');

        return new Response($json);
    }

}
